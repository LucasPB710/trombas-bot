import youtube_dl


def get_url(url):
    # url = "https://www.youtube.com/watch?v=Lr0UOKd1dd0"
    YDL_OPTIONS = {    'format': 'bestaudio/best',
        'outtmpl': 'downloads/%(extractor)s-%(id)s-%(title)s.%(ext)s',
        'restrictfilenames': True,
        'noplaylist': True,
        'nocheckcertificate': True,
        'ignoreerrors': False,
        'logtostderr': False,
        'quiet': True,
        'no_warnings': True,
        'default_search': 'auto',
        'source_address': '0.0.0.0'  # ipv6 addresses cause issues sometimes
        }
    ydl =  youtube_dl.YoutubeDL(YDL_OPTIONS)
    _url = url
    info = ydl.extract_info(_url, download=False)
    return info['url']
